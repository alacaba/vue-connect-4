import Vue from 'vue'
const RED    = 'red'
const BLACK  = 'black'
const EMPTY  = 'empty'
const PLAY   = 'play'
const OVER   = 'over'
const Bus    = new Vue({})
const key    = (row, col) => `${row}${col}`
const range  = num => [...Array(num).keys()]
const cssUrl = id => `url(#${id})`
const min    = num => Math.max(num - 3, 0)
const max    = (num, max) => Math.min(num+3, max)
const titleize = text => text[0].toUpperCase() + text.slice(1, text.length)

export {
  RED,
  BLACK,
  EMPTY,
  Bus,
  PLAY,
  OVER,
  key,
  range,
  cssUrl,
  min,
  max,
  titleize
}

